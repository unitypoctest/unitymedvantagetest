trigger updateContactLead on Account (after update){
    
    Set<Id> accIdSet = new Set<Id>();
    List<Contact> conList =  new List <Contact>();
    for(Account a : trigger.new){
     system.debug('@@New'+trigger.new);
        if(a.Industry == 'Electronics'  && a.Rating == 'Warm')
            accIdSet.add(a.Id);
            
        
    }
    
    system.debug('@@accSet'+accIdSet );
    
    for(Contact c : [Select Id,LeadSource, AccountId from Contact Where AccountId in: accIdSet ]){
        
        if(c.LeadSource == '' || c.LeadSource == null){
                c.LeadSource = 'Web';
                conList.add(c);
                    
                    }
    }
    
        update conList;
        system.debug('@@conList'+conList);

}