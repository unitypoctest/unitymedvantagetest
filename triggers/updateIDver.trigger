trigger updateIDver on Customer__c (before insert) {

   /*** 
   Syntax for after trigger 
   set<Id> cId = new Set<Id>();
    List<Customer__c> cuslIst = new List<Customer__c >();
    for(Customer__c c : trigger.new){
    
        if(c.Contract_Signed__c == 'Yes')
        
        cId.add(c.Id);
    
    }
    
    for(Customer__c  cus :[Select Id , I_D_Verification__c   from Customer__c   where Id in: cId]){
    
        cus.I_D_Verification__c   = true;
        cuslIst.add(cus);
    
    
    }
    
    update cuslIst;
    
    *****/
    
    for(Customer__c c : trigger.new){
    
        if(c.Contract_Signed__c == 'Yes')
        
        c.I_D_Verification__c   = true;
    
    }
}